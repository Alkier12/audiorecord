#!/usr/bin/env php
<?php

    chdir(__DIR__ . '/..');
    date_default_timezone_set('Europe/Moscow');

    $lastTag = exec('git tag --sort=v:refname | grep r.' . date('Y.m.d'));
    $newTag = 'r.' . date('Y.m.d') . '.1';

    if ($lastTag != '') {
        if (preg_match('/r\.\d{4}\.\d{2}\.\d{2}\.(\d+)/i', $lastTag, $matches)) {
            $newTag = 'r.' . date('Y.m.d') . '.' . (intval($matches[1]) + 1);
        }
    }

    echo 'New tag ' . $newTag . "\n";

    exec('git tag ' . $newTag);
    exec('git push origin ' . $newTag);

    echo 'Finish.' . "\n";
