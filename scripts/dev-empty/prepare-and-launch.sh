#!/bin/bash
set -e
# let's do it in private :)
cd "$(dirname "$0")"

usage()
{
    echo "usage: $0 [-i user_id -l user_login -e user@example.com [-k key.pub|-p password] [-d subdomain] | [-h]]"
    exit
}


user_id=
login=
email=
password=`pwgen -1 -n 12`
domain=`pwgen -01A 10`
newsetup=0
#ipaddress=`facter ipaddress_eth0`
ipaddress="87.249.36.140"
key=/dev/null

while [ "$1" != "" ]; do
    case $1 in
        -i | --id )             shift
                                user_id=$1
                                ;;
        -l | --login )          shift
                                login=$1
                                ;;
        -d | --domain )         shift
                                domain=$1
                                ;;
        -e | --email )          shift
								email=$1
                                ;;
        -p | --password )       shift
								password=$1
                                ;;
        -k | --key )       		shift
								key=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [[ -z $user_id || -z $login || -z $email ]]; then 
	usage
fi


re='^[0-9]+$'
if ! [[ $user_id =~ $re ]]; then 
	usage
fi

if [[ $user_id == 45 ]]; then # nonono:)
	usage
fi

if [[ $login =~ $re ]]; then 
	usage
fi

# get key in var
key_raw=`cat ${key}`
user_id_z=$(printf '%02d' ${user_id})
# create dot-env
echo -e "GITLAB_USER_ID=${user_id}\nGITLAB_USER_ID_Z=${user_id_z}\nCOMPOSE_PROJECT_NAME=novator-${user_id}" > .env
echo "composer down"
#current=`docker ps --format '{{.Names}}'| grep web-${user_id}-novator`

#if [[ $current != "" ]]; then
# stop dockers
docker compose down
#fi

# if it's empty, copy nginx default configs from dist
mkdir -p config/{supervisor,nginx}/user-${user_id}
mkdir -p code/user-${user_id}/beanstalkd
chmod -R a+w config/{supervisor,nginx}/user-${user_id}
chmod -R a+w code/user-${user_id}
chmod -R a+w code/user-${user_id}/beanstalkd


if [ -z "$(ls -A config/nginx/user-${user_id})" ]; then
	# new setup
	newsetup=1
	cp -a config/nginx/dist/* config/nginx/user-${user_id}/
	echo "configuring nginx"
	sed -i "s/DOMAIN/${domain}.nvtr.pro/g" config/nginx/user-${user_id}/api.novator.conf
	echo ${domain} > config/nginx/user-${user_id}/domain.txt
fi

if [ -z "$(ls -A config/supervisor/user-${user_id})" ]; then
        # new setup
        newsetup=1
        echo "configuring supervisor"
        cp -a config/supervisor/dist/* config/supervisor/user-${user_id}/
fi

if test -f config/nginx/user-${user_id}/domain.txt; then
	domain=`cat config/nginx/user-${user_id}/domain.txt`
fi



#echo "syncing code"
#rsync -ax ${CI_PROJECT_DIR} code/user${USER_ID}/

# launch docker-compose
echo "Launching API stage for ${login}."
#docker-compose build --no-cache
#docker compose up --build --force-recreate --always-recreate-deps -V -d
docker compose up --build -d

echo "adding user"
docker exec -e SSH_MASTER_USER=${login} -e SSH_MASTER_PASS=${password} -e SSH_KEY="${key_raw}" web-${user_id}-novator /usr/local/bin/user.sh || true

if [[ $newsetup == 1 ]]; then 
	docker exec -it postgres-${user_id}-novator /usr/bin/createdb -U postgres novator || true
fi

echo "sending email with password"

echo -e "Hello ${login}!\nStage for you is ready.\n\n\tLogin: 'ssh ${login}@owl.nvtr.pro -p 22${user_id_z}'\n\tPassword: ${password}\n\n\thttps://${domain}.nvtr.pro:44${user_id_z}\n\n Please add to your /etc/hosts:\n${ipaddress} ${domain}.nvtr.pro"
echo -e "Hello ${login}!\nStage for you is ready.\n\n\tLogin: 'ssh ${login}@owl.nvtr.pro -p 22${user_id_z}'\n\tPassword: ${password}\n\n\thttps://${domain}.nvtr.pro:44${user_id_z}\n\n Please add to your /etc/hosts:\n${ipaddress} ${domain}.nvtr.pro" | mail -s "API stage ${domain}.nvtr.pro for ${login} is ready" ${email}
echo "resetting env"
echo > .env
echo "done"

