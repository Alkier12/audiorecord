#!/bin/bash
set -e
 
#printf "\n\033[0;44m---> Starting the SSH server.\033[0m\n"
 
#service ssh start
#service ssh status

printf "\n\033[0;44m---> Starting nginx.\033[0m\n"
 
service nginx start
service nginx status

printf "\n\033[0;44m---> Starting php-fpm.\033[0m\n"
 
#service php7.4-fpm start
#service php7.4-fpm status
service php8.1-fpm start
service php8.1-fpm status


# launched via supervisor
#printf "\n\033[0;44m---> Starting beanstalkd.\033[0m\n"
 
#service beanstalkd start
#service beanstalkd status

printf "\n\033[0;44m---> Starting supervisord.\033[0m\n"
 
service supervisor start
service supervisor status

 
exec "$@"

