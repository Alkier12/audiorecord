#!/bin/bash
#set -e

printf "\n\033[0;44m---> Creating SSH master user.\033[0m\n"

#echo "Setting pass for ${SSH_MASTER_USER} to ${SSH_MASTER_PASS}"

useradd -m -d /home/${SSH_MASTER_USER} -G www-data ${SSH_MASTER_USER} -s /bin/bash
mkdir -p /var/www/api.novator
chown ${SSH_MASTER_USER}:www-data /var/www/api.novator
echo "${SSH_MASTER_USER}:${SSH_MASTER_PASS}" | chpasswd

echo 'PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin"' >> /home/${SSH_MASTER_USER}/.profile

#echo "${SSH_MASTER_USER} ALL=NOPASSWD:/bin/rm" >> /etc/sudoers
#echo "${SSH_MASTER_USER} ALL=NOPASSWD:/bin/mkdir" >> /etc/sudoers
#echo "${SSH_MASTER_USER} ALL=NOPASSWD:/bin/chown" >> /etc/sudoers
#echo "${SSH_MASTER_USER} ALL=NOPASSWD:/usr/sbin/useradd" >> /etc/sudoers
#echo "${SSH_MASTER_USER} ALL=NOPASSWD:/usr/sbin/deluser" >> /etc/sudoers
#echo "${SSH_MASTER_USER} ALL=NOPASSWD:/usr/sbin/chpasswd" >> /etc/sudoers
echo "${SSH_MASTER_USER} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

echo "AllowUsers ${SSH_MASTER_USER}" >> /etc/ssh/sshd_config
if ! [[ -z ${SSH_KEY} ]]; then
	echo "UsePAM no" >> /etc/ssh/sshd_config
	mkdir -p /home/${SSH_MASTER_USER}/.ssh
	echo "${SSH_KEY}" > /home/${SSH_MASTER_USER}/.ssh/authorized_keys
fi

exec "$@"
