#!/usr/bin/env bash

HOST=${HOST:="https://gitlab.com/api/v4/projects/"}
TARGET_BRANCH="dev"
ASSIGNEE_ID="3450512"   # Denis Lapaev

GL_URL="https://gitlab.com/devnovator/"

ASANA_HOST="https://app.asana.com/api/1.0/"
ASANA_TASK_REGEXP="https://app.asana.com/[0-9]+/[0-9]+/[0-9]+"
ASANA_ID_REGEXP="^[0-9]+$"

# The description of our new MR, we want to remove the branch after the MR has
# been closed
MR_BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": \"true\",
    \"title\": \"Draft: ${CI_COMMIT_MESSAGE//[$'\t\r\n']/ }\",
    \"author_id\":\"${GITLAB_USER_ID}\",
    \"assignee_id\":\"${ASSIGNEE_ID}\"
}";

# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LISTMR=`curl --silent "${HOST}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${GITLAB_TOKEN}"`;
COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | wc -l`;

# No MR found, let's create a new one
if [ ${COUNTBRANCHES} -eq "0" ]; then

    MR_RAW=`curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" --header "PRIVATE-TOKEN:${GITLAB_TOKEN}" --header "Content-Type: application/json" --data "${MR_BODY}"`;
    echo "New merge request: ${MR_RAW}";

    MR_ID=`echo ${MR_RAW} | egrep -o "\"iid\":[0-9]+" | egrep -o "[0-9]+"`;
    MR_URL="${GL_URL}${CI_PROJECT_NAME}/-/merge_requests/${MR_ID}";

    echo "Opened a new merge request: ${MR_URL}";

    # Add MR link to Asana
    for TASK_MATCH in `echo ${CI_COMMIT_MESSAGE} | egrep -o ${ASANA_TASK_REGEXP}`
    do
        TASK_ID=${TASK_MATCH##*/}
        echo "Founded task id in commit message: ${TASK_ID}";

        if ! [[ $TASK_ID =~ $ASANA_ID_REGEXP ]] ; then
            continue
        fi

        COMMENT_RAW=`curl -s --data-urlencode "text=${CI_PROJECT_NAME}: ${MR_URL}" -H "Authorization: Bearer ${ASANA_TOKEN}" "${ASANA_HOST}tasks/${TASK_ID}/stories"`;
        COMMENT_ID=`echo ${COMMENT_RAW} | egrep -o "\"data\":{\"gid\":\"[0-9]+\"" | egrep -o "[0-9]+"`;
        PINNED_RAW=`curl -s -X PUT --data-urlencode "is_pinned=true" -H "Authorization: Bearer ${ASANA_TOKEN}" "${ASANA_HOST}stories/${COMMENT_ID}"`;

        echo "Commented with new merge request: ${COMMENT_ID}";
    done

    exit;
fi
