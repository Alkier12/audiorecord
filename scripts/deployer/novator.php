<?php

namespace Deployer;

desc('Copy .env config');
task('novator:env',  function () {
    run('cp {{release_path}}/env/{{alias}}.env {{release_path}}/.env');
});

desc('Application down');
task('novator:down', function () {
    if (run('{{bin/php}} {{release_path}}/artisan migrate:check') !== 'No pending migrations') {
        invoke('artisan:down');
    }
});

desc('Application up');
task('novator:up', function () {
    invoke('artisan:up');
});

desc('Migrate database');
task('novator:migrate', function () {
    if (run('{{bin/php}} {{release_path}}/artisan migrate:check') !== 'No pending migrations') {
        invoke('artisan:migrate');
        info('artisan:migrate ok');
    }
});

desc('Restart supervisorctl');
task('novator:supervisorctl:restart', function () {
    run('sudo /usr/bin/supervisorctl restart back:');
    info('supervisorctl restart ok');
});

desc('Gitlab Strategy');
task('strategy:gitlab', [
    'hook:start',
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'hook:ready',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:cleanup',
    'hook:done',
]);
fail('strategy:gitlab', 'deploy:failed');

desc('Upload a given folder to your hosts');
task('upload', function () {
    $configs = array_merge_recursive(get('upload_options'), [
        'options' => ['--delete']
    ]);

    upload('{{upload_path}}/', '{{release_path}}', $configs);
});
