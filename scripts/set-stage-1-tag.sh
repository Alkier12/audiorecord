#!/usr/bin/env php
<?php

    chdir(__DIR__ . '/..');
    date_default_timezone_set('Europe/Moscow');

    $lastTag = exec('git tag --sort=v:refname | grep s1.' . date('Y.m.d'));
    $newTag = 's1.' . date('Y.m.d') . '.1';

    if ($lastTag != '') {
        if (preg_match('/s1\.\d{4}\.\d{2}\.\d{2}\.(\d+)/i', $lastTag, $matches)) {
            $newTag = 's1.' . date('Y.m.d') . '.' . (intval($matches[1]) + 1);
        }
    }

    echo 'New tag ' . $newTag . "\n";

    exec('git tag ' . $newTag);
    exec('git push origin ' . $newTag);

    echo 'Finish.' . "\n";
