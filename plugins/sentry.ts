import * as Sentry from "@sentry/vue";
import {httpClientIntegration} from "@sentry/integrations";

export default defineNuxtPlugin((nuxtApp) => {
  const {vueApp} = nuxtApp;
  const router = useRouter();
  const config = useRuntimeConfig();

  if (useRuntimeConfig().public.environment === "production") {
    Sentry.init({
      app: [vueApp],
      dsn: "https://feaca6a2b4740c52cd217b11d088114f@sentry.nvtr.pro/5",
      environment: config.public.environment,
      release: `dilogo@${config.public.release}`,
      attachProps: true,
      trackComponents: true,
      integrations: [
        httpClientIntegration({
          failedRequestStatusCodes: [[400, 599]],
        }),
      ],
    });
  }
});
