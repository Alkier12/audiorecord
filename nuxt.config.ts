// https://nuxt.com/docs/api/configuration/nuxt-config

import * as path from "path";

const modules = [] as Array<string | Array<string | object>>;

// here we need to use process.env because of generate command
if (process.env.ENV === "production") {
  modules.push([
    "yandex-metrika-module-nuxt3",
    {
      id: 95102261,
      webvisor: true,
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      trackHash: true,
    },
  ]);
}

export default defineNuxtConfig({
  compatibilityDate: "2024-04-03",

  nitro: {
    prerender: {
      crawlLinks: false,
      routes: ["/"],
    },
  },
 

  

  css: ["~/assets/styles/default.less", "~/assets/fonts/fonts.less"],

  runtimeConfig: {
    public: {
      environment: process.env.ENV,
      release: process.env.RELEASE,
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
      },
    },
  },

  app: {
    head: {
      meta: [
        {
          name: "viewport",
          content: "width=device-width, initial-scale=1.0",
        },
        {
          charset: "utf-8",
        },
      ],
      script: [],
    },
  },

  experimental: {
    payloadExtraction: false,
  },

  ssr: false,
  sourcemap: {
    server: true,
    client: true,
  },

  modules: modules as any,
});
